<?php


class clanok
{
    private $titulok;
    private $info;
    private $obrazok;
    private $id;
    private $text;
    private $trailer;

    public function __construct($id, $titulok, $info, $obrazok, $text, $trailer)
    {
        $this->id = $id;
        $this->titulok = $titulok;
        $this->info = $info;
        $this->obrazok = $obrazok;
        $this->text = $text;
        $this->trailer = $trailer;

    }

    /**
     * @return mixed
     */
    public function getTitulok()
    {
        return $this->titulok;
    }

    /**
     * @param mixed $titulok
     */
    public function setTitulok($titulok)
    {
        $this->titulok = $titulok;
    }

    /**
     * @return mixed
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * @param mixed $info
     */
    public function setInfo($info)
    {
        $this->info = $info;
    }

    /**
     * @return mixed
     */
    public function getObrazok()
    {
        return $this->obrazok;
    }

    /**
     * @param mixed $obrazok
     */
    public function setObrazok($obrazok)
    {
        $this->obrazok = $obrazok;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param mixed $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return mixed
     */
    public function getTrailer()
    {
        return $this->trailer;
    }

    /**
     * @param mixed $trailer
     */
    public function setTrailer($trailer)
    {
        $this->trailer = $trailer;
    }





}

