<?php

interface IStorage
{
    
    public function saveUser(Pouzivatel $param);

    public function LoadUsers();

    public function LoadReviews();

    public function SaveReview(Review $param);

    public function deleteReview($id);

    public function updateReview(Review $param);




}