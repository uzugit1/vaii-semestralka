<?php
session_start();
?>

<!DOCTYPE html>
<html lang="sk">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Svet Hier</title>


    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="icon" type="image/png" href="pics/favicon.png"/>
    <link href="css/cssUvod.css" rel="stylesheet">

</head>

<body onload="change('carousel.html')">

<nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="uvod.php">Svet Hier</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">

                <li class="nav-item">
                    <a class="nav-link" href="novinky.php">Novinky</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="hodnotenia.php">Recenzie</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="chat.php">Chat</a>
                </li>

                <?php if (!isset($_SESSION['loggedin'])) { ?>
                    <li class="nav-item">
                        <a class="nav-link" href="register.php">Login</a>
                    </li>
                <?php } ?>
                <?php if (isset($_SESSION['loggedin'])) { ?>
                    <li class="nav-item">
                        <a class="nav-link"  href="logout.php">Logout</a>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
</nav>

<button type="submit" onclick="change('carousel.html')"  class="btn btn-primary zmena">1</button>
<button type="submit" onclick="change('carousel2.html')"  class="btn btn-primary zmena">2</button>



<div id="carousel">

<script>

    function change(prvok) {
        const xhr = new XMLHttpRequest();
        const carousel = document.getElementById('carousel');
        xhr.onload = function () {
            if (this.status === 200) {
                carousel.innerHTML = xhr.responseText;
            } else {
                console.warn("error");
            }

        };
        xhr.open('get', prvok);
        xhr.send();
    }


</script>
</div>


<!-- Bootstrap core JavaScript -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>


</body>

</html>