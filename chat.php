<?php
session_start();

?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <title>Chat</title>
        <link rel="icon" type="image/png" href="pics/favicon.png"/>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="css/chatcss.css"/>
        <link rel="stylesheet" href="css/cssUvod.css"/>
    </head>
<body>

    <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top">
        <div class="container">
            <a class="navbar-brand" href="uvod.php">Svet Hier</a>
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                    data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                    aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="novinky.php">Novinky</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="hodnotenia.php">Recenzie</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="chat.php">Chat</a>
                    </li>
                    <?php if (!isset($_SESSION['loggedin'])) { ?>
                        <li class="nav-item">
                            <a class="nav-link" href="register.php">Login</a>
                        </li>
                    <?php } ?>
                    <?php if (isset($_SESSION['loggedin'])) { ?>
                        <li class="nav-item">
                            <a class="nav-link" href="logout.php">Logout</a>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </nav>

<?php
if (!isset($_SESSION['loggedin'])) { ?>
    <script type="text/javascript">

        var r = confirm("Pre chatovanie sa prihláste");
        if ((r == false) || (r == true)) {
            window.location.href = "uvod.php"
        }

    </script>
<?php }
else {
?>
    <div id="wrapper">
        <div id="menu">
        </div>

        <div id="chatbox">
            <?php
            if (file_exists("log.html") && filesize("log.html") > 0) {
                $contents = file_get_contents("log.html");
                echo $contents;
            }
            ?>
        </div>

        <form name="message" action="chat.php">
            <input name="usermsg" type="text" id="usermsg"/>
            <input name="submitmsg" type="submit" id="submitmsg" value="Pošli"/>
        </form>
    </div>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
        // jQuery Document
        $(document).ready(function () {
            $("#submitmsg").click(function () {
                var clientmsg = $("#usermsg").val();
                $.post("post.php", {text: clientmsg});
                $("#usermsg").val("");
                return false;
            });

            function loadLog() {
                var oldscrollHeight = $("#chatbox")[0].scrollHeight - 20;

                $.ajax({
                    url: "log.html",
                    cache: false,
                    success: function (html) {
                        $("#chatbox").html(html);


                        var newscrollHeight = $("#chatbox")[0].scrollHeight - 20;
                        if (newscrollHeight > oldscrollHeight) {
                            $("#chatbox").animate({scrollTop: newscrollHeight}, 'normal');
                        }
                    }
                });
            }

            function deleteLog() {

                $.ajax({
                    url: 'deleteLog.php',
                    cache: false,
                    success: function (html) {

                    }
                });
            }
            setInterval(loadLog, 2000);
            setInterval(deleteLog , 30000);
        });
    </script>
    </body>

    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    </html>

    <?php
}
?>