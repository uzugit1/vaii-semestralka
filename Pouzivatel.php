<?php
class Pouzivatel
{
    private $username;
    private $password;
    private $id;

    public function __construct($id, $username, $password)
    {
        $this->username = $username;
        $this->password = $password;
        $this->id = $id;
    }


    public function getUsername()
    {
        return $this->username;
    }


    public function setUsername($username)
    {
        $this->username = $username;
    }


    public function getPassword()
    {
        return $this->password;
    }


    public function setPassword($password)
    {
        $this->password = $password;
    }


    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }


}
