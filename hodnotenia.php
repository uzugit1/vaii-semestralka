
<?php
session_start();
require "Review.php";
require "IStorage.php";
require "DBStorage.php";


$storage = new DBStorage();
$moje = false;

if (isset($_POST['odoslat'])) {
    if (isset($_SESSION['loggedin'])){
        $storage->SaveReview(new Review(null, $_POST['nazov'], $_POST['obsah'], $_POST['hodnotenie'], $_SESSION["username"]));
        unset($_POST['nazov']);
        header("Location: " . $_SERVER['PHP_SELF']);
    }
    else{ ?>
        <script type="text/javascript">

            var r = confirm("Pre pridanie sa prihláste.");
            if ((r == false) || (r == true)) {
                window.location.href = "hodnotenia.php"
            }
        </script>
    <?php }
}

if (isset($_POST["Vymaz"]) && (isset($_SESSION['loggedin']))) {
    foreach ($storage->LoadReviews() as $review) {
        if ((($review->getid() == $_POST['id']) && ($review->getUsername() == $_SESSION["username"])) || isset($_SESSION['adminloggedin'])) {
            $moje = true;
        }
    }
    if ($moje == true) {

        $storage->deleteReview($_POST['id']);
    } else { ?>
        <script type="text/javascript">

            var r = confirm("Môžete zmazať iba svoju vlastnú recenziu.");
            if ((r == false) || (r == true)) {
                window.location.href = "hodnotenia.php"
            }
        </script>
    <?php }

} else if (isset($_POST["Vymaz"])) { ?>
    <script type="text/javascript">

        var r = confirm("Pre vykonanie sa prihláste.");
        if ((r == false) || (r == true)) {
            window.location.href = "hodnotenia.php"
        }
    </script>

<?php }

?>
<!DOCTYPE html>
<html>

<head>
    <title>Recenzie</title>
    <link rel="icon" type="image/png" href="pics/favicon.png"/>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/cssUvod.css" rel="stylesheet">
    <link href="css/cssHodnotenia.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="jquery-3.5.1.min.js"></script>

</head>

<br>

<h1>Pridajte aj vy recenziu svojej obľúbenej hry</h1>

<nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="uvod.php">Svet Hier</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="novinky.php">Novinky</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="hodnotenia.php">Recenzie</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="chat.php">Chat</a>
                </li>
                <?php if (!isset($_SESSION['loggedin'])) { ?>
                    <li class="nav-item">
                        <a class="nav-link" href="register.php">Login</a>
                    </li>
                <?php } ?>
                <?php if (isset($_SESSION['loggedin'])) { ?>
                    <li class="nav-item">
                        <a class="nav-link" href="logout.php">Logout</a>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
</nav>


<br>
<br>
<form method="post" action="hodnotenia.php">
    <div class="form-group form1">
        <label class="nazovcss" >Nazov</label>
        <input name="nazov" type="text" class="form-control" placeholder="" minlength="5" required>
    </div>

    <div class="form-group form2">
        <label class="hodnoteniecss" >Hodnotenie</label>
        <input name="hodnotenie" type="number" class="form-control" placeholder="" min = "0" max = "10" required>
    </div>
    <div class="form-group form3">
        <label class="obsahcss" >Detaily</label>
        <textarea name="obsah" class="form-control" rows="3" minlength="10" required></textarea>
    </div>
    <button type="submit" name="odoslat" class="btn btn-primary">Odoslať</button>
</form>
<table id="myTable" class="paginated display table table-dark">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Nickname</th>
        <th scope="col">Názov</th>
        <th scope="col">Hodnotenie</th>
    </tr>
    </thead>
    <tbody>

    <?php foreach ($storage->LoadReviews() as $review) { ?>

        <tr>
            <th scope="row"><?php echo $review->getid(); ?></th>
            <td><?php echo $review->getUsername(); ?></td>
            <td><?php echo $review->getNazov() ?></td>
            <td><?php echo $review->getHodnotenie() ?></td>
            <td>
                <form method="post" action="hodnotenia.php">
                    <input onclick="return confirm('Naozaj chcete recenziu vymazať?')" class="tlacitko" type="submit" name="Vymaz" value="Vymaž"/>
                    <input type="hidden" name="id" value="<?php echo $review->getid(); ?>"/>
                </form>
                <form method="post" action="hodnotenie.php">
                    <input  class="tlacitko" type="submit" name="Zobraz" value="Zobraz"/>
                    <input type="hidden" name="id1" value="<?php echo $review->getid(); ?>"/>
                </form>
            </td>

        </tr>

    <?php }
    ?>

    </tbody>
</table>
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
</body>
</html>