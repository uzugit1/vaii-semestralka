<?php

require "Pouzivatel.php";
require "IStorage.php";
require "DBStorage.php";

$username = $password = $confirm_password = "";
$username_err = $password_err = $confirm_password_err = "";
$username_taken = false;
$storage = new DBStorage();


if ($_SERVER["REQUEST_METHOD"] == "POST") {

    if (empty(trim($_POST["username"]))) {
        $username_err = "Zadajte používateľské meno";
    } else {

        foreach ($storage->LoadUsers() as $pouzivatel) {

            if ($pouzivatel->getUsername() == trim($_POST["username"])) {
                $username_taken = true;
            }
        }
        if ($username_taken == true) {
            $username_err = "Toto meno sa už používa";
        } else {
            $username = trim($_POST["username"]);
        }

    }

    if (empty(trim($_POST["password"]))) {
        $password_err = "Zadajte heslo";
    } elseif (strlen(trim($_POST["password"])) < 6) {
        $password_err = "Heslo musí mať minimálne 6 znakov.";
    } else {
        $password = trim($_POST["password"]);
    }

    if (empty(trim($_POST["confirm_password"]))) {
        $confirm_password_err = "Potvrďte heslo";
    } else {
        $confirm_password = trim($_POST["confirm_password"]);
        if (empty($password_err) && ($password != $confirm_password)) {
            $confirm_password_err = "Heslá sa nezhodujú";
        }
    }

    if (empty($username_err) && empty($password_err) && empty($confirm_password_err)) {


        $param_username = $username;
        $param_password = password_hash($password, PASSWORD_DEFAULT);


        header("location: login.php");

        $storage->saveUser(new Pouzivatel(null, $param_username, $param_password));


    }
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Register</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="icon" type="image/png" href="pics/favicon.png"/>
    <link href="css/cssregister.css" rel="stylesheet">
    <link href="css/cssUvod.css" rel="stylesheet">
    <style type="text/css">

    </style>
</head>
<body>

<nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="uvod.php">Svet Hier</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="novinky.php">Novinky</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="hodnotenia.php">Recenzie</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="chat.php">Chat</a>
                </li>
                <?php if (!isset($_SESSION['loggedin'])) { ?>
                    <li class="nav-item">
                        <a class="nav-link" href="register.php">Login</a>
                    </li>
                <?php } ?>
                <?php if (isset($_SESSION['loggedin'])) { ?>
                    <li class="nav-item">
                        <a class="nav-link" href="logout.php">Logout</a>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
</nav>

<div class="wrapper">
    <h2>Registrácia</h2>
    <p>Vyplňte tieto údaje pre vytvorenie účtu.</p>
    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
        <div class="form-group <?php echo (!empty($username_err)) ? 'has-error' : ''; ?>">
            <label>Používateľské meno</label>
            <input type="text" name="username" class="form-control" value="<?php echo $username; ?>">
            <span class="help-block"><?php echo $username_err; ?></span>
        </div>
        <div class="form-group <?php echo (!empty($password_err)) ? 'has-error' : ''; ?>">
            <label>Heslo</label>
            <input type="password" name="password" class="form-control" value="<?php echo $password; ?>">
            <span class="help-block"><?php echo $password_err; ?></span>
        </div>
        <div class="form-group <?php echo (!empty($confirm_password_err)) ? 'has-error' : ''; ?>">
            <label>Zopakujte heslo</label>
            <input type="password" name="confirm_password" class="form-control"
                   value="<?php echo $confirm_password; ?>">
            <span class="help-block"><?php echo $confirm_password_err; ?></span>
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-primary" value="Potvrdiť">
        </div>
        <p>Máte už účet ? <a href="login.php">Prihláste sa tu</a>.</p>
    </form>
</div>
<!-- Bootstrap core JavaScript -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
</body>
</html>
