<?php

session_start();


if (isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true) {
    header("location: uvod.php");
    exit;
}

require "Pouzivatel.php";
require "IStorage.php";
require "DBStorage.php";


$username = $password = "";
$username_err = $password_err = "";
$username_found = false;
$password_correct = false;
$storage = new DBStorage();


if ($_SERVER["REQUEST_METHOD"] == "POST") {

    // Check if username is empty
    if (empty(trim($_POST["username"]))) {
        $username_err = "Zadajte meno";
    } else {
        $username = trim($_POST["username"]);
    }


    if (empty(trim($_POST["password"]))) {
        $password_err = "Zadajte heslo.";
    } else {
        $password = trim($_POST["password"]);
    }

    if (empty($username_err) && empty($password_err)) {

        foreach ($storage->LoadUsers() as $pouzivatel) {
            if ($pouzivatel->getUsername() == $username) {
                $username_found = true;
            }
            if (password_verify($password, $pouzivatel->getPassword())) {
                $password_correct = true;
            }
        }
        if ($username_found == false) {
            $username_err = "Nenašiel sa účet s týmto menom";
        }
        if ($password_correct == true) {
            // Password is correct, so start a new session
            session_start();

            // Store data in session variables
            if ($username == "admin"){
                $_SESSION["adminloggedin"] = true;
            }
            $_SESSION["loggedin"] = true;
            $_SESSION["username"] = $username;

            // Redirect user to welcome page
            header("location: uvod.php");
        } else {
            $password_err = "Zadali ste nesprávne heslo";
        }


    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="icon" type="image/png" href="pics/favicon.png"/>
    <link href="css/cssregister.css" rel="stylesheet">
    <link href="css/cssUvod.css" rel="stylesheet">
    <style type="text/css">

    </style>
</head>
<body>
<nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="uvod.php">Svet Hier</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="novinky.php">Novinky</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="hodnotenia.php">Recenzie</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="chat.php">Chat</a>
                </li>
                <?php if (!isset($_SESSION['loggedin'])) { ?>
                    <li class="nav-item">
                        <a class="nav-link" href="register.php">Login</a>
                    </li>
                <?php } ?>
                <?php if (isset($_SESSION['loggedin'])) { ?>
                    <li class="nav-item">
                        <a class="nav-link" href="logout.php">Logout</a>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
</nav>

<div class="wrapper">
    <h2>Login</h2>
    <p>Zadajte údaje pre prihlásenie.</p>
    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
        <div class="form-group <?php echo (!empty($username_err)) ? 'has-error' : ''; ?>">
            <label>Používateľské meno</label>
            <input type="text" name="username" class="form-control" value="<?php echo $username; ?>">
            <span class="help-block"><?php echo $username_err; ?></span>
        </div>
        <div class="form-group <?php echo (!empty($password_err)) ? 'has-error' : ''; ?>">
            <label>Heslo</label>
            <input type="password" name="password" class="form-control">
            <span class="help-block"><?php echo $password_err; ?></span>
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-primary" value="Login">
        </div>
        <p>Nemáte účet? <a href="register.php">Registrujte sa tu</a>.</p>
    </form>
</div>
<!-- Bootstrap core JavaScript -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
</body>
</html>
