<?php
require "Review.php";
require "IStorage.php";
require "DBStorage.php";

$storage = new DBStorage();
session_start();
$myReview = null;
$moje = false;

?>
<?php if (isset($_SESSION['loggedin'])) { ?>
    <!DOCTYPE html>
    <html>

    <head>
        <title>Recenzie</title>
        <link rel="icon" type="image/png" href="pics/favicon.png"/>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/cssUvod.css" rel="stylesheet">
        <link href="css/cssHodnotenie.css" rel="stylesheet">

    </head>
    <body>
    <?php

    if (isset($_POST['id1'])) {
        foreach ($storage->LoadReviews() as $review)
            if ($review->getid() == $_POST['id1']) {
                $myReview = $review;
            }
    }

    if (isset($_POST['id2'])) {
        foreach ($storage->LoadReviews() as $review)
            if ($review->getid() == $_POST['id2']) {
                $myReview = $review;
            }
    }
    ?>

    <?php

    if (isset($_POST["upravit"])) {
        foreach ($storage->LoadReviews() as $review) {
            if ((($review->getid() == $_POST['id2']) && ($review->getUsername() == $_SESSION["username"])) || isset($_SESSION['adminloggedin'])) {
                $moje = true;
            }
            }
            if ($moje == true) {
                $myReview->setNazov($_POST['nazov']);
                $myReview->setHodnotenie($_POST['hodnotenie']);
                $myReview->setObsah($_POST['obsah']);
                $storage->updateReview($myReview);
            } else { ?>
                <script type="text/javascript">

                    var r = confirm("Môžete upraviť iba svoju vlastnú recenziu.");
                    if ((r == false) || (r == true)) {
                        window.location.href = "hodnotenia.php"
                    }

                </script>

                <?php
            }
    }
    ?>
    <div class="container">
        <div class="row">
            <div class="col-md-4 mx-auto center1">

                <form method="post" action="hodnotenie.php">
                    <div class="form-group form1">
                        <label class="nazovcss">Nazov</label>
                        <input name="nazov" type="text" class="form-control"
                               value="<?php echo $myReview->getNazov() ?>" minlength="5" required>

                    </div>

                    <div class="form-group form2">
                        <label class="hodnoteniecss">Hodnotenie</label>
                        <input name="hodnotenie" type="number" class="form-control"
                               value="<?php echo $myReview->getHodnotenie() ?>" min = "0" max = "10" required>
                    </div>
                    <div class="form-group form3">
                        <label class="obsahcss">Detaily</label>
                        <textarea name="obsah" class="form-control"
                                  rows="3" minlength="10" required><?php echo $myReview->getObsah() ?></textarea>
                    </div>
                    <br>
                    <button type="submit" name="upravit" class="btn btn-primary">Upravit</button>
                    <input type="hidden" name="id2" value="<?php echo $myReview->getid(); ?>"/>
                    <a href="hodnotenia.php" class="btn btn-primary">Späť</a>
                </form>
            </div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    </body>
    </html>
<?php } else { ?>
    <script type="text/javascript">

        var r = confirm("Pre zobrazenie sa prihláste.");
        if ((r == false) || (r == true)) {
            window.location.href = "hodnotenia.php"
        }
    </script>
    <?php
} ?>

