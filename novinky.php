<?php
session_start();
require "clanok.php";
require "IStorage.php";
require "DBStorage.php";

$storage = new DBStorage();

if (isset($_POST['pridat'])) {
    $storage->saveClanok(new clanok(null, $_POST['titulok'], $_POST['info'], $_POST['obrazok'], $_POST['text'], $_POST['trailer']));
    unset($_POST['pridat']);
    header("Location: " . $_SERVER['PHP_SELF']);
}

if (isset($_POST["odstranit"])) {
    foreach ($storage->LoadClanky() as $clanok) {
        if ($clanok->getid() == $_POST['id']) {
            $storage->deleteClanok($_POST['id']);
        }
    }
}

?>
<!DOCTYPE html>
<html lang="sk">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Svet Hier</title>


    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="icon" type="image/png" href="pics/favicon.png"/>
    <link href="css/cssUvod.css" rel="stylesheet">
    <link href="css/cssNovinky.css" rel="stylesheet">
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</head>

<body>

<?php
if (isset($_SESSION['adminloggedin'])) { ?>
    <button onclick="openNav()" id="pridanie" type="submit" name="pridanie" class="btn btn-primary">Pridať</button>
<?php }
?>

<nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="uvod.php">Svet Hier</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="novinky.php">Novinky</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="hodnotenia.php">Recenzie</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="chat.php">Chat</a>
                </li>
                <?php if (!isset($_SESSION['loggedin'])) { ?>
                    <li class="nav-item">
                        <a class="nav-link" href="register.php">Login</a>
                    </li>
                <?php } ?>
                <?php if (isset($_SESSION['loggedin'])) { ?>
                    <li class="nav-item">
                        <a class="nav-link" href="logout.php">Logout</a>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
</nav>

<div id="myNav" class="overlay">

    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
    <br>
    <br>
    <br>

    <form method="post" action="novinky.php">
        <div class="form-group form1">
            <label class="nazovcss">Titulok</label>
            <input name="titulok" type="text" class="form-control">

        </div>

        <div class="form-group form2">
            <label class="hodnoteniecss">Info</label>
            <input name="info" type="text" class="form-control"
            >
        </div>
        <div class="form-group form2">
            <label class="hodnoteniecss">Obrazok</label>
            <input name="obrazok" type="text" class="form-control"
            >
        </div>
        <div class="form-group form3">
            <label class="obsahcss">Text</label>
            <textarea name="text" class="form-control" rows="3"></textarea>
        </div>
        <div class="form-group form2">
            <label class="hodnoteniecss">Trailer</label>
            <input name="trailer" type="text" class="form-control"
            >
        </div>
        <br>
        <button type="submit" name="pridat" class="btn btn-primary">Pridat</button>

    </form>

</div>


<?php foreach ($storage->LoadClanky() as $clanok) { ?>

    <div class="card mb-3">
        <div class="row no-gutters">
            <div class="col-md-4">
                <img src="<?php echo $clanok->getObrazok() ?>" class="card-img" alt="cp">
            </div>
            <div class="col-md-8">
                <div class="card-body">
                    <h5 class="card-title"><?php echo $clanok->getTitulok() ?></h5>
                    <p class="card-text"><?php echo $clanok->getInfo() ?>.</p>
                    <form method="post" action="actualClanok.php">
                        <button type="submit" name="precitaj" class="btn btn-primary">Prečítaj</button>
                        <input type="hidden" name="id" value="<?php echo $clanok->getId(); ?>"/>
                    </form>
                    <br>
                    <form method="post" action="novinky.php">
                    <?php
                    if (isset($_SESSION['adminloggedin'])) { ?>
                        <button type="submit" name="odstranit" class="btn btn-primary">Odstrániť</button>
                        <input type="hidden" name="id" value="<?php echo $clanok->getId(); ?>"/>
                        </form>
                    <?php } ?>
                </div>

            </div>

        </div>

    </div>

<?php } ?>


<script>
    function openNav() {
        document.getElementById("myNav").style.height = "100%";
    }

    /* Close */
    function closeNav() {
        document.getElementById("myNav").style.height = "0%";
    }
</script>


</body>
</html>