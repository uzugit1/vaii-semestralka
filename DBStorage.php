<?php


class DBStorage implements IStorage
{
    /**
     * @var PDO
     */
    private $pdo;

    public function __construct()
    {
        $this->pdo = new PDO("mysql:host=localhost;dbname=my_database", "root", "dtb456");
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }


    public function saveClanok(clanok $param)
    {
        $statement = $this->pdo->prepare("INSERT INTO clanky (id, titulok,info, obrazok, text, trailer) values (?,?,?,?,?,?)");
        $statement->execute([$param->getId(), $param->getTitulok(),$param->getInfo(), $param->getObrazok(), $param->getText(), $param->getTrailer()]);
    }

    public function LoadClanky()
    {
        $vysledok = [];

        $f = $this->pdo->query("SELECT * FROM clanky");

        foreach ($f as $prvok) {
            $vysledok[] = new clanok($prvok['id'], $prvok['titulok'], $prvok['info'], $prvok['obrazok'], $prvok['text'], $prvok['trailer']);
        }

        return $vysledok;
    }

    public function deleteClanok($id)
    {
        $stmt = $this->pdo->prepare("DELETE FROM clanky WHERE id =:id");
        $stmt->bindParam(':id', $id);
        $stmt->execute();
    }

    public function saveUser(Pouzivatel $param)
    {
        $statement = $this->pdo->prepare("INSERT INTO users (username, password) values (?,?)");
        $statement->execute([$param->getUsername(), $param->getPassword()]);
    }

    public function LoadUsers()
    {
        $vysledok = [];

        $f = $this->pdo->query("SELECT * FROM users");

        foreach ($f as $prvok) {
            $vysledok[] = new Pouzivatel($prvok['id'], $prvok['username'], $prvok['password']);
        }

        return $vysledok;
    }


    public function LoadReviews()
    {
        $result = [];

        $r = $this->pdo->query("SELECT * FROM hodnotenia");

        foreach ($r as $item) {
            $result[] = new Review($item['id'], $item['nazov'], $item['obsah'], $item['hodnotenie'], $item['username']);
        }

        return $result;
    }


    public function SaveReview(Review $param)
    {
        $statement = $this->pdo->prepare("INSERT INTO hodnotenia (nazov,obsah, hodnotenie, username) values (?,?,?,?)");
        $statement->execute([$param->getNazov(), $param->getObsah(), $param->getHodnotenie(), $param->getusername()]);
    }

    public function deleteReview($id)
    {
        $stmt = $this->pdo->prepare("DELETE FROM hodnotenia WHERE id =:id");
        $stmt->bindParam(':id', $id);
        $stmt->execute();
    }

    public function updateReview(Review $param)
    {

        $sql = "UPDATE hodnotenia SET nazov=?, obsah=?, hodnotenie=? WHERE id=?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute([$param->getNazov(), $param->getObsah(), $param->getHodnotenie(), $param->getid()]);
    }

}
