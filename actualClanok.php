<?php

session_start();
require "clanok.php";
require "IStorage.php";
require "DBStorage.php";

$storage = new DBStorage();
$_SESSION["idClanku"]=$_POST['id'];
$actualClanok = null;
?>



<!DOCTYPE html>
<html lang="sk">
<head>
    <link rel="shortcut icon" type="image/x-icon" href="../vaii-semestralka/pics/favicon.png">
    <meta charset="UTF-8">
    <title>Novinka</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
          integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
            integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx"
            crossorigin="anonymous"></script>
    <link rel="stylesheet" href="css/clanok.css">
    <link rel="icon" type="image/png" href="pics/favicon.png"/>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/cssUvod.css" rel="stylesheet">
    <link href="css/cssHodnotenia.css" rel="stylesheet">

</head>
<body>

<?php
foreach ($storage->LoadClanky() as $clanok) {
    if ($clanok->getId() == $_SESSION["idClanku"]){
        $actualClanok = $clanok;
    }
}

?>
<nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="uvod.php">Svet Hier</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="novinky.php">Novinky</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="hodnotenia.php">Recenzie</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="chat.php">Chat</a>
                </li>
                <?php if (!isset($_SESSION['loggedin']) )  {?>
                    <li class="nav-item">
                        <a class="nav-link" href="register.php">Login</a>
                    </li>
                <?php } ?>
                <?php if ( isset($_SESSION['loggedin']) )  {?>
                    <li class="nav-item">
                        <a class="nav-link" href="logout.php">Logout</a>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
</nav>

<div class="conCP">
    <div class="nadpis">
        <p>
            <?php echo $actualClanok->getTitulok() ?>
        <p>
    </div>
    <div class="picture">
        <img src="<?php echo $actualClanok->getObrazok() ?>" class="mx-auto img-fluid " alt="cp1">
    </div>


    <div class="text">

        <?php echo $actualClanok->getText() ?>

        <p class="farba">
            Trailer:
        </p>
    </div>
    <div class="container">
        <iframe class="video"  src="<?php echo $actualClanok->getTrailer() ?>"  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
</div>

</body>
</html>