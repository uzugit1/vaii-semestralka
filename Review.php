<?php


class Review
{
    private $nazov;
    private $hodnotenie;
    private $obsah;
    private $id;
    private $username;

    public function __construct($id,$nazov, $obsah, $hodnotenie, $username)
    {
        $this->nazov = $nazov;
        $this->hodnotenie = $hodnotenie;
        $this->obsah = $obsah;
        $this->id = $id;
        $this->username = $username;
    }


    public function getNazov()
    {
        return $this->nazov;
    }

    public function setNazov($nazov)
    {
        $this->nazov = $nazov;
    }

    public function getHodnotenie()
    {
        return $this->hodnotenie;
    }


    public function setHodnotenie($hodnotenie)
    {
        $this->hodnotenie = $hodnotenie;
    }
    public function getObsah()
    {
        return $this->obsah;
    }


    public function setObsah($obsah)
    {
        $this->obsah = $obsah;
    }

    public function getid()
    {
        return $this->id;
    }
    public function getusername()
    {
        return $this->username;
    }


}
